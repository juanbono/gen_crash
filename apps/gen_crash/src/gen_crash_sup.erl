%%%-------------------------------------------------------------------
%% @doc gen_crash top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(gen_crash_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: #{id => Id, start => {M, F, A}}
%% Optional keys are restart, shutdown, type, modules.
%% Before OTP 18 tuples must be used to specify a child. e.g.
%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    SupFlags = #{strategy => one_for_one, intensity => 1, period => 5},
    GreeterSpec = #{id => greeter, start => {greeter, start_link, ["Abel Pintos"]}},
    FriendSpec = #{id => friend, start => {friend, start_link, ["Osvaldo Laport"]}},
    {ok, {SupFlags, [GreeterSpec]}}.

%%====================================================================
%% Internal functions
%%====================================================================

