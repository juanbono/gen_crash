-module(friend).

-behaviour(gen_server).

% public functions
-export([start_link/1, greet/1]).

% callbacks
-export([handle_call/3, init/1]).

%%---------------------------------------------------------------
%% Client
%%---------------------------------------------------------------
start_link(Name) ->
  gen_server:start_link(?MODULE, [Name], []).

greet(Name) ->
  gen_server:cast(?MODULE, {greet, Name}, 2000).

%%---------------------------------------------------------------
%% Callbacks
%%---------------------------------------------------------------
handle_call({greet, Name}, _From, State = #{name := FriendName}) ->
  io:format("Hi ~p! My name is ~p.~n", [Name, FriendName]),
  {reply, ok, State}.

init(Name) -> 
  {ok, #{name => Name}}.
