-module(looper).

-export([start/1]).

start(N) ->
  Pid = spawn(fun () ->
    Loop = fun (F, Steps) ->
	         receive
               die -> erlang:display("Me muero");
			   Msg when Steps > 0 ->
			     io:format("Recibi ~p, Steps: ~p~n", [Msg, Steps]),
				 F(F, Steps - 1);
			   _ -> erlang:display("Steps == 0")
			 end
		   end,
	Loop(Loop, N)
  end),
  Pid.
