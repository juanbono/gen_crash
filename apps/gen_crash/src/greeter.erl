-module(greeter).

-behaviour(gen_server).

% public functions
-export([start_link/1, greet/1]).

% callbacks
-export([handle_info/2, handle_call/3, init/1]).

start_link(Name) ->
  {ok, Pid} = gen_server:start_link(?MODULE, [Name], []),
  register(greeter, Pid),
  {ok, Pid}.

greet(Name) ->
  gen_server:call(greeter, {greet, Name}, 2000).

handle_call({greet, Name}, _From, State = #{name := GreeterName, friends := Friends}) ->
  io:format("Hi ~p! My name is ~p.~n They are my friends!~n", [Name, GreeterName]),
  lists:foreach(fun(Friend) -> friend:greet(Friend, Name) end, Friends),
  {reply, ok, State}.

init(Name) -> 
  {ok, #{name => Name, friends => []}}.

handle_info(Msg, State) ->
  io:format("Message ~p received!~n", [Msg]), 
  {noreply, State}.
